// Count the occurrence of a given character in a string.
// String Reference: "The Rime of the Ancient Mariner" by Samuel Taylor Coleridge

public class OccurrenceInString {

    // Count occurrences of char c in String string
    private static int search(String string, char c) {

        int occurrence = 0;

        // For every character match in string, increment occurrence
        for (int i = 0; i < string.length(); i++) {
            if (c == string.charAt(i)) {
                occurrence++;
            }
        }

        return occurrence;
    }

    // Main
    public static void main(String[] args) {

        // String with unknown occurrences of a given character
        String string = "Water, water, everywhere, nor any drop to drink.";
        // Character to find within string
        char c = 'r';

        System.out.println("\'" + c + "\' occurs " + search(string, c) + " times in \"" + string + "\"");
    }
}
