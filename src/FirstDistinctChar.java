// Print the first non-repeated character from a string.

public class FirstDistinctChar {

    private static char search(String string) {
        int j = 0;
        char c;
        boolean distinct = true;

        for (int i = 0; i < string.length(); i++) {
             c = string.charAt(j);
             if (string.charAt(i) == c) {
                 distinct = false;
             }
            if (distinct) { return c; }
        }
        return 'X';
    }

    // Return true if all characters are repeated
    // Return false otherwise
    private static boolean isRepeated(String string) {
        return false;
    }

    // Main
    public static void main(String[] args) {
        // String with unknown occurrences of a given character
        // String is all lower-case so the answer isn't the leading letter of a sentence or proper noun
        String string = "Friends, Romans, countrymen, lend me your ears".toLowerCase();

        if (!isRepeated(string)) {
            System.out.println("The first non-repeated character was " + search(string));
        } else {
            System.out.println("All characters are repeated.");
        }
    }
}

