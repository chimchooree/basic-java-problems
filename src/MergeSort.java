import java.util.Arrays;
import java.util.List;

public class MergeSort {

    private static List<Integer> sort(List<Integer> newList) {
        return newList;
    }

    // Print given List
    private static void printList(List<Integer> newList) {
        int length = newList.size();

        // Add commas and spaces to list for readability
        // Print list + break line
        for (int i = 0; i < length; i++) {
            System.out.print(newList.get(i));
            if (i < length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("");
    }

    // Main
    public static void main(String[] args) {
        // List to be Sorted
        List<Integer> list = Arrays.asList(33,9,7,5,3,2,-6);

        //Print unsorted list
        printList(list);

        //Print sorted list
        printList(sort(list));
    }
}
