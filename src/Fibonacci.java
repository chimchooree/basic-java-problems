// Print the number at a given integer's place in the Fibonacci sequence using Recursion

public class Fibonacci {

    // Find the i'th Fibonacci number using recursion
    public static int fib(int i) {
        if (i <= 1) {
            return i;
        }
        return fib(i-1)+fib(i-2);
    }

    // Return the ordinal number for the given integer
    private static String makeOrdinal(int i) {
        switch(i) {
            case 1:
                return "1st";
            case 2:
                return "2nd";
            case 3:
                return "3rd";
            default:
                return i + "th";
        }
    }

    // Main
    public static void main(String[] args) {

        // Find the num'th Fibonacci number; must be positive
        int num = 7;

        System.out.println("The " + makeOrdinal(num) + " Fibonacci number is " + fib(num));
    }

}
