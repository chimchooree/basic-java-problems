import java.util.Arrays;
import java.util.List;

// Implement a Bubble Sort algorithm.

public class BubbleSort {

    // Sort given list using a Bubble Sort Algorithm
    private static List<Integer> sort(List<Integer> newList) {
        int pos;
        for (int j = newList.size() - 1; j > 0; j--) {
            pos = 0;

            int large = newList.get(0);
            for (int i = 1; i < newList.size(); i++) {
                newList.set(pos++, Math.min(newList.get(i), large));
                newList.set(pos, Math.max(newList.get(i), large));
                large = Math.max(newList.get(i), large);
            }
        }
        return newList;
    }

    // Print given List
    private static void printList(List<Integer> newList) {
        int length = newList.size();

        // Add commas and spaces to list for readability
        // Print list + break line
        for (int i = 0; i < length; i++) {
            System.out.print(newList.get(i));
            if (i < length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("");
    }

    // Main
    public static void main(String[] args) {
        // List to be Sorted
        List<Integer> list = Arrays.asList(9,7,5,3,2,-6);

        //Print unsorted list
        printList(list);

        //Print sorted list
        printList(sort(list));
    }
}
